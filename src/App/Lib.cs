﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App
{
    public class Lib
    {
        private readonly int x;

        public Lib(int x)
        {
            this.x = x;
        }

        public string Greeting => $"hi! I'm {this.x}";

        public int Add(int y) => this.x + y;
    }
}
