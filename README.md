### Getting Started
To make a Bitbucket Pipeline, all you have to do is go to the "Pipelines" tab in your repo and select whatever Bitbucket's flavor of the month for "add pipeline" is. Hopefully it's somewhat obvious. Pick a template, or start with the default, you could probably even create your own, it just needs to be called `bitbucket-pipelines.yaml`

it's a `.yaml` file, so indentation and spacing is significant, don't let that bite you.

### Image
All Bitbucket Pipelines use a docker image. Find the image you want to use for your app

Here, we're running on .NET core 3.1, so we find the image here https://hub.docker.com/_/microsoft-dotnet-sdk

We're also using a node image, which we find here https://hub.docker.com/_/node

Bitbucket pipelines does not support Windows right now (see https://support.atlassian.com/bitbucket-cloud/docs/limitations-of-bitbucket-pipelines/)
so we have to use bash/Linux to run tests ... Your unit tests are self contained right ...>:)

### Steps and Scripts
Give each step a name so it can be identified in the pipeline.
Each `step:` can run scripts. 

you can write the scripts directly in the `bitbucket-pipelines.yml`
```yaml
step:
	- echo "running build"
	- dotnet build
```
or you can use an actual script (has to be a bash script for now)
```yaml
step:
	- ./build.sh
```
if you write a script like `build.sh` you might have to make it executable by git. This is a little foreign to me
but you might get "permission denied" errors if you don't.
running `git update-index --chmod=+x build.sh` where `build.sh` is the name of your script should work

you can also mix the direct scripts with actual bash scripts as I've done in this repo.

you can even run scripts that require a different runtime by specifying the runtime image to use directly in the step
```yaml
- step:
	name: Deploy to production
	image: python:3.7.2
	script:
	  - python deploy-to-production.py
```

### Where to go next
For configuring the `bitbutcket-pipelines.yml`, check out https://support.atlassian.com/bitbucket-cloud/docs/configure-bitbucket-pipelinesyml/
it will give you a much better idea of what you can do with it.