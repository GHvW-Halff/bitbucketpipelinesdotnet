const adder = require('./index');

test('adds 1 + 2 to equal 3', () => {
    const add1 = adder(1);

    const result = add1(2);

    expect(result).toBe(3);
});