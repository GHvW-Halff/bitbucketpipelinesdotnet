const adder = (x) => (y) => x + y;

module.exports = adder;