﻿using Xunit;

namespace App.Tests
{
    public class LibTests
    {

        [Fact]
        public void AddTest()
        {
            var lib = new Lib(10);

            var result = lib.Add(20);

            Assert.Equal(30, result);
        }

        [Fact]
        public void GreetingTest()
        {
            var lib = new Lib(100);

            var result = lib.Greeting;

            Assert.Equal("hi! I'm 100", result);
        }
    }
}
